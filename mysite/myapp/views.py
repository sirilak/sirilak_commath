

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse

def index(req):
    return render(req, 'myapp/index.html')

def ed(req):
    return render(req, 'myapp/404.html')

def blank(req):
    return render(req, 'myapp/blank.html')

def buttons(req):
    return render(req, 'myapp/buttons.html')

def cards(req):
    return render(req, 'myapp/cards.html')

def charts(req):
    return render(req, 'myapp/charts.html')

def forgotpassword(req):
    return render(req, 'myapp/forgot-password.html')

def login(req):
    return render(req, 'myapp/login.html')

def register(req):
    return render(req, 'myapp/register.html')

def tables(req):
    return render(req, 'myapp/tables.html')

def utilitiesanimation(req):
    return render(req, 'myapp/utilities-animation.html')

def utilitiesborder(req):
    return render(req, 'myapp/utilitiesborder.html')

def utilitiescolor(req):
    return render(req, 'myapp/utilities-color.html')

def utilitiesother(req):
    return render(req, 'myapp/utilities-other.html')