"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from myapp import views

urlpatterns = [
    path('', views.index),
    path('admin/', admin.site.urls),
    path('ed/', views.ed),
    path('blank/', views.blank),
    path('buttons/', views.buttons),
    path('cards/', views.cards),
    path('charts/', views.charts),
    path('forgotpassword/', views.forgotpassword),
    path('login/', views.login),
    path('register/', views.register),
    path('utilitiesanimation/', views.utilitiesanimation),
    path('utilitiesborder/', views.utilitiesborder),
    path('utilitiescolor/', views.utilitiescolor),
    path('utilitiesother/', views.utilitiesother),
]